/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   management.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 20:20:09 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/07/09 20:20:11 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_print_tab(t_fdf *info)
{
	int i, j;
	j = 0;
	while (j < info->y)
	{
		i = 0;
		while (i < info->x)
		{
			ft_putnbr(info->map[j][i]);
			ft_putstr(" ");
			i++;
		}
		ft_putchar('\n');
		j++;
	}	
}

void	ft_modif_tab(char **s)
{
	int i;
	char *tmp;

	i = 0;
	while ((*s)[i] && (*s)[i] != ',')
		i++;
	tmp = *s;
	*s = ft_strsub(*s, 0, i + 1);
	free(tmp); 
}

int		*ft_convert_char(char **tab, int len)
{
	int *ret;
	int j;

	ret = (int *)malloc(sizeof(int) * len);
	j = 0;
	while (tab[j])
	{
		if (ft_strchr(tab[j], ','))
			ft_modif_tab(&tab[j]);
		ret[j] = ft_atoi(tab[j]);
		j++;
	}
	return (ret);
}

void	ft_stock_map(t_fdf *info)
{
	char *line;
	char **tab;
	int j;

	info->map = (int **)malloc(sizeof(int *) * info->y);
	info->fd = ft_open_file(info->name);
	j = 0;
	while (get_next_line(info->fd, &line))
	{
		tab = ft_strsplit(line, ' ');
		free(line);
		info->map[j++] = ft_convert_char(tab, info->x);
	}
	close(info->fd);
}

int		ft_management(t_fdf *info)
{
	void *mlx_ptr;
	void *mlx_win;
	ft_stock_map(info);
	//ft_print_tab(info);
	mlx_ptr = mlx_init();
	mlx_win = mlx_new_window(mlx_ptr, 1000, 1000, "new_win");
	mlx_pixel_put(mlx_ptr, mlx_win, 500, 500, 0xFF0000);
	mlx_pixel_put(mlx_ptr, mlx_win, 501, 501, 0xFF0000);
	mlx_pixel_put(mlx_ptr, mlx_win, 502, 502, 0xFF0000);
	mlx_loop(mlx_ptr);
	return (1);
}