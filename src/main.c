/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 15:08:25 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/07/09 20:18:40 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_num_of_char(char *line)
{
	int i;
	int num;

	i = 0;
	num = 0;
	while (line[i])
	{
		while (line[i] && line[i] == ' ')
			i++;
		if (line[i] == '\0')
			break ;
		while (line[i] && line[i] != ' ')
			i++;
		if ((line[i] == ' ' && line[i - 1] != ' ') || (line[i] == '\0'))
			num++;
	}
	return (num);
}

int		ft_file_is_valid(t_fdf *info)
{
	char	*line;
	int		ret;
	int		num;


	if (get_next_line(info->fd, &line) == -1)
		return (-1);
	num = ft_num_of_char(line);
	info->x = num;
	free(line);
	info->y = 1;
	while ((ret = get_next_line(info->fd, &line)))
	{
		if (ret == -1)
			return (-1);
		if (num != ft_num_of_char(line))
		{	
			free(line);
			return (0);
		}
		info->y++;
		free(line);
	}
	close(info->fd);
	return (1);
}

int		ft_open_file(char *name)
{
	int fd;

	if ((fd = open(name, O_RDWR))  == -1)
	{
		perror("");
		close(fd);
		exit(0);
	}
	return (fd);
}

int main(int ac, char **av)
{
	t_fdf	info;
	int		ret;

	if (ac == 2)
	{
		info.fd = ft_open_file(av[1]);
		ret = ft_file_is_valid(&info);
		if (ret == -1)
			ft_putstr("get_next_line error\n");
		if (ret == 0)
		{
			ft_putstr("the map is not valid\n");
			return (0);
		}
		info.name = ft_strdup(av[1]);
		ft_management(&info);
	}
	else if (ac > 2)
		ft_putstr("Too many arguments\n");	
	return (0);
}
