# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/07/09 17:22:15 by hlamhidr          #+#    #+#              #
#    Updated: 2019/07/09 18:05:55 by hlamhidr         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
LIB = src/get_next_line/libft/libft.a
LIB_PATH = src/get_next_line/libft/
SRC = src/get_next_line/get_next_line.c src/main.c src/management.c
FLAG = -Wall -Wextra -Werror -lmlx -framework OpenGL -framework AppKit 
INC = inc

all: $(NAME)
$(NAME):
	@make -C $(LIB_PATH)
	@gcc $(FLAG) -o $(NAME) -I $(INC) $(SRC) $(LIB) 

clean:
	@make clean -C $(LIB_PATH)

fclean: clean
	@make fclean -C $(LIB_PATH)
	@/bin/rm -f $(NAME)

re: fclean all
