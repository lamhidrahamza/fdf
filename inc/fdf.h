/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 15:22:05 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/07/09 20:15:14 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "get_next_line.h"
# include "libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <mlx.h>

typedef struct s_fdf
{
	int x;
	int y;
	int fd;
	int **map;
	char *name;
}				t_fdf;

int		ft_open_file(char *name);
int		ft_management(t_fdf *info);


#endif
